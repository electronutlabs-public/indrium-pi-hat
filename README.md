# Indrium Pi Hat

*This is an experimental product still under development*

![Indrium pi-hat](Indrium-pi-hat.jpg)

## What is Indrium Pi HAT ?

Indrium Pi HAT - a Nordic nRF52840 shield for Raspberry Pi 3b+ that
converts it into a low power wireless gateway for BLE and 802.15.4 based
networks. It has a NFC reader on it which can be used for 
commisioning / provisiong devices.

Features:

   * MDBT50 BLE/802.15.4 module - Nordic semiconductor nRF52840
     * Can be programmed with the Pi using Openocd and Pi's GPIOs
     * Can run a qualified BLE controller as HCI-Uart, from Zephyr RTOS project for extra BLE on your pi
     * Can run Openthread's NCP firmware over SPI to make your Pi into a Thread gateway
     * Can make your Pi into a 6lowpan gateway
 * NFC reader using PN532
     * Can be used as a commisioning / provisioning mechanism for a Thread network for example
 * 2 buttons for your Pi
 * RGB LED as status indicator

## Schematic

You can find the schematic for this board at this [link](https://gitlab.com/electronutlabs-public/indrium-pi-hat/raw/master/pi_gateway_hat_v0.2_schematic.pdf?inline=false).

## Code Repository

https://gitlab.com/electronutlabs-public/indrium-pi-hat

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).